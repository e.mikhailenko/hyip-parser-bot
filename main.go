package main

import (
	"fmt"
	"github.com/joho/godotenv"
	"gopkg.in/telebot.v3"
	"main/parsers"
	"os"
	"time"
)

func init() {
	if err := godotenv.Load(); err != nil {
		_ = fmt.Errorf("no .env file found")
	}
}

func main() {
	bot, err := telebot.NewBot(telebot.Settings{
		Token:  os.Getenv("SECRET_TOKEN"),
		Poller: &telebot.LongPoller{Timeout: 10 * time.Second},
	})

	if err != nil {
		_ = fmt.Errorf(err.Error())
	}

	var (
		selectorChooseHyip = &telebot.ReplyMarkup{}

		btnCoinex     = selectorChooseHyip.Data("Coinex", "coinex")
		btnDollarBill = selectorChooseHyip.Data("DollarBill", "dollarBill")
	)

	selectorChooseHyip.Inline(
		selectorChooseHyip.Row(btnCoinex, btnDollarBill),
	)

	bot.Handle("/start", func(context telebot.Context) error {
		return context.Send("Здравствуйте! Выберите HYIP для получения подробной информации:", selectorChooseHyip)
	})

	bot.Handle(&btnCoinex, func(context telebot.Context) error {
		return context.Send(parsers.GetCoinexInfo())
	})

	bot.Handle(&btnDollarBill, func(context telebot.Context) error {
		return context.Send("dollar bill parser")
	})

	bot.Start()
}
