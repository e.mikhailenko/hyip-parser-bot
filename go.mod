module main

go 1.18

require (
	github.com/joho/godotenv v1.4.0
	gopkg.in/telebot.v3 v3.0.0
)

require (
	github.com/PuerkitoBio/goquery v1.8.0 // indirect
	github.com/andybalholm/cascadia v1.3.1 // indirect
	golang.org/x/net v0.0.0-20210916014120-12bc252f5db8 // indirect
)
