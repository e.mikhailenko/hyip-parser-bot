package parsers

import (
	"fmt"
	"github.com/PuerkitoBio/goquery"
	"io"
	"net/http"
)

func check(err error) {
	if err != nil {
		_ = fmt.Errorf(err.Error())
	}
}

func GetCoinexInfo() string {
	url := "https://www.hyip.biz/details/coinex-tp.com"
	response, err := http.Get(url)
	defer func(Body io.ReadCloser) {
		err := Body.Close()
		if err != nil {
			_ = fmt.Errorf(err.Error())
		}
	}(response.Body)

	check(err)

	if response.StatusCode > 400 {
		fmt.Println("Status code: ", response.StatusCode)
	}

	document, err := goquery.NewDocumentFromReader(response.Body)
	check(err)

	hyipExplorerInfo := document.Find("table.odd").Find("#monm1")
	statusHE := hyipExplorerInfo.Find("td b a").Text()
	statusUpdatedHE := hyipExplorerInfo.Find("td").Find("div[class='date sub']").Text()
	lastPayoutHE := hyipExplorerInfo.Find("td div.date").Find("b").Text()
	outputInfoHE := "*****HYIP Explorer Info*****" + "\n" + "Status: " + statusHE +
		"\n" + "Updated: " + statusUpdatedHE + "\n" + "Last payout: " + lastPayoutHE + "\n\n"

	invesTracingInfo := document.Find("table.odd").Find("#monm19")
	statusIT := invesTracingInfo.Find("td b span[style='color:#00B300;']").Text()
	statusUpdatedIT := invesTracingInfo.Find("td").Find("div[class='date sub']").Text()
	lastPayoutIT := invesTracingInfo.Find("td div.date").Find("b").Text()
	outputInfoIT := "*****INVEST TRACING Info*****" + "\n" + "Status: " + statusIT +
		"\n" + "Updated: " + statusUpdatedIT + "\n" + "Last payout: " + lastPayoutIT

	fullOutputInfo := outputInfoHE + outputInfoIT

	return fullOutputInfo
}
